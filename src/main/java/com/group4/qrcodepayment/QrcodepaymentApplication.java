package com.group4.qrcodepayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QrcodepaymentApplication {

    public static void main(String[] args) {
        SpringApplication.run(QrcodepaymentApplication.class, args);
    }

}
