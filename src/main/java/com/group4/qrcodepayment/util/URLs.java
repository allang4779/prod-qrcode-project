package com.group4.qrcodepayment.util;

public class URLs {
    public static final String REGISTER = "/auth/register";
    public static final String LOGIN = "/auth/login";
    public static final String HOME = "/";
}
